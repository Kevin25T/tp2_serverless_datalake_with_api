# Créer un bucket aws_s3_bucket

resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket = "s3-job-offer-bucket-carlotirou"
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = "s3-job-offer-bucket"
    Environment = "Dev"
  }
}

# Créer un répertoire aws_s3_bucket_object avec job_offers/raw/

resource "aws_s3_bucket_object" "object" {
  bucket = "s3-job-offer-bucket-carlotirou"
  key    = "raw"
  source = "/dev/null"
}

# Créer un event aws_s3_bucket_notification pour trigger la lambda

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.s3_job_offer_bucket.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.test_lambda.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "raw/"
    filter_suffix       = ".csv"
  }
}
