# Créer le rôle pour la lambda avec aws_iam_role

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Créer une lambda fonction aws_lambda_function

resource "aws_lambda_function" "test_lambda" {

  filename      = "empty_lambda_code.zip"
  handler       = "lambda_main_app.lambda_handler"
  function_name = "tp2devops"
  role = "${aws_iam_role.iam_for_lambda.arn}"
  runtime = "python3.7"

  source_code_hash = "${filebase64sha256("empty_lambda_code.zip")}"

  environment {
    variables = {
      foo = "bar"
    }
  }
}

# Créer une aws_iam_policy pour le logging qui sera ajouté au rôle de la lambda

resource "aws_iam_policy" "lambda_logging" {
  name = "lambda_logging"
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Attacher la policy de logging au rôle de la lambda avec aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role = "${aws_iam_role.iam_for_lambda.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

# Attacher la policy de AmazonS3FullAccess au rôle de la lambda avec aws_iam_role_policy_attachment

# Autoriser la lambda à être déclenchée par un event s3 avec aws_lambda_permission

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.test_lambda.id}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${aws_s3_bucket.s3_job_offer_bucket.arn}"
}
